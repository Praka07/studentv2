package com.rucproject.exception;

public class GlobalBusinessException extends RuntimeException {
    private String code;

    public GlobalBusinessException() {
    }

    public GlobalBusinessException(String code, String message) {
        super(message);
        this.code = code;
    }

    public GlobalBusinessException(String message) {

    }

	public String getCode() {
        return code;
    }
}
