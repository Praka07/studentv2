package com.rucproject.exception;

public class StudentCRUDException extends RuntimeException {
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public StudentCRUDException() {
        super();
    }

    public StudentCRUDException(String message) {
        super(message);
        this.message=message;
    }
	
}
