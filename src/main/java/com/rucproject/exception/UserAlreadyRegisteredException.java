package com.rucproject.exception;

public class UserAlreadyRegisteredException extends GlobalBusinessException {

    public UserAlreadyRegisteredException() {
        super();
    }

    public UserAlreadyRegisteredException(String code, String message) {
        super(code, message);
    }
}
