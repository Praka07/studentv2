package com.rucproject.exception.config;

import com.rucproject.exception.GlobalBusinessException;
import com.rucproject.exception.StudentCRUDException;
import com.rucproject.exception.StudentNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @SuppressWarnings("rawtypes")
	@ExceptionHandler({GlobalBusinessException.class})
    public ResponseEntity handleGlobalBusinessException(GlobalBusinessException e, Locale locale) {
        return ResponseEntity
            .badRequest()
            .body(new ErrorResponse(e.getCode(), e.getMessage()));
    }
    @SuppressWarnings("rawtypes")
	@ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity handleAccessDeniedException(AccessDeniedException e, Locale locale) {
        System.out.println("AccessDeniedException occurred..!"+e);
        return ResponseEntity
            .status(HttpStatus.FORBIDDEN)
            .body(new ErrorResponse("401", e.getMessage()));
    }
    
	@ExceptionHandler({StudentNotFoundException.class})
    public ResponseEntity handleStudentNotFoundException(StudentNotFoundException e) {
        System.out.println("StudentNotFoundException occurred1234.. student not found! "+e);
        return  ResponseEntity
            .status(HttpStatus.NOT_FOUND)
//            .body(new ErrorResponse(e.getMessage(),e.getMessage()));
            .body(new ErrorResponse("401",e.getMessage()));
    }
	
	
	@ExceptionHandler({StudentCRUDException.class})
    public ResponseEntity handleStudentCUDNotPerformedException(StudentNotFoundException e, Locale locale) {
        System.out.println("StudentNotFoundException occurred.. student not found! "+e);
        return ResponseEntity
            .status(HttpStatus.NOT_FOUND)
            .body(new ErrorResponse("400", e.getMessage()));
    }
	
}
