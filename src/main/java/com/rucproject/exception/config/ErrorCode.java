package com.rucproject.exception.config;

public interface ErrorCode {
    interface Application {
        String ERROR_SERVER_ERROR = "500";
        String USER_ALREADY_REGISTERED = "1000";
    }
}
