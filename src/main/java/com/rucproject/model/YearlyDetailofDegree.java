package com.rucproject.model;

import java.util.List;


public class YearlyDetailofDegree
{
    private String level;
    private List<Module> coreModulesTaken;
    private List<Module> optionalModulesTaken;

    public YearlyDetailofDegree(){
        super();
    }

    public YearlyDetailofDegree(String level, List<Module> coreModulesTaken, List<Module> optionalModulesTaken) {
        this.level = level;
        this.coreModulesTaken = coreModulesTaken;
        this.optionalModulesTaken = optionalModulesTaken;
    }

    public List<Module> getCoreModulesTaken() {
        return coreModulesTaken;
    }

    public void setCoreModulesTaken(List<Module> coreModulesTaken) {
        this.coreModulesTaken = coreModulesTaken;
    }

    public List<Module> getOptionalModulesTaken() {
        return optionalModulesTaken;
    }

    public void setOptionalModulesTaken(List<Module> optionalModulesTaken) {
        this.optionalModulesTaken = optionalModulesTaken;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    }
