package com.rucproject.model;

import org.springframework.data.mongodb.core.mapping.Document;
import java.util.List;

@Document
public class DegreeDetails {

    private List<YearlyDetailofDegree> careerPathLevelDetails;

    public DegreeDetails()
    {
        super();
    } 

    public DegreeDetails(List<YearlyDetailofDegree> careerPathLevelDetails) {
        this.careerPathLevelDetails = careerPathLevelDetails;
    }

    public List<YearlyDetailofDegree> getCareerPathLevelDetails() {
        return careerPathLevelDetails;
    }

    public void setCareerPathLevelDetails(List<YearlyDetailofDegree> careerPathLevelDetails) {
        this.careerPathLevelDetails = careerPathLevelDetails;
    }

}
