package com.rucproject.model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document
public class Module {

    private String moduleTitle;
    private String yearOfModule;
    private long percentageAwarded;
    private LocalDate dateTaken;
    private long percentageOfExam;
    private long percentageOfCoursework;
    private String moduleCode;

    public Module(){
        super();
    }

    public Module(String yearOfModule,String moduleTitle, long percentageAwarded, LocalDate dateTaken, long percentageOfExam, long percentageOfCoursework) {
        this.yearOfModule=yearOfModule;
        this.moduleTitle=moduleTitle;
        this.percentageAwarded = percentageAwarded;
        this.dateTaken = dateTaken;
        this.percentageOfExam = percentageOfExam;
        this.percentageOfCoursework = percentageOfCoursework;
    }

    public String getYearOfModule() { return yearOfModule; }

    public void setYearOfModule(String yearOfModule) {
        this.yearOfModule = yearOfModule;
    }

    public long getPercentageOfExam() {
        return percentageOfExam;
    }

    public void setPercentageOfExam(long percentageOfExam) {
        this.percentageOfExam = percentageOfExam;
    }

    public String getModuleTitle() {
        return moduleTitle;
    }

    public void setModuleTitle(String moduleTitle) {
        this.moduleTitle = moduleTitle;
    }

    public long getPercentageOfCoursework() {
        return percentageOfCoursework;
    }

    public void setPercentageOfCoursework(long percentageOfCoursework) {
        this.percentageOfCoursework = percentageOfCoursework;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public long getPercentageAwarded(){return percentageAwarded;};

    public LocalDate getDateTaken() {return dateTaken; }

    public void setDateTaken(LocalDate dateTaken) { this.dateTaken = dateTaken; }

}
