package com.rucproject.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Document
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String firstName;
	private String lastName;

	private String yearEnrolled;
	private DegreeDetails degreeDetails;

	enum Level {
		LEVEL_5,
		LEVEL_6,
		LEVEL_7
	}

	public String getDegreeName() {
		return degreeName;
	}

	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}

	private String degreeName;
	public String getYearEnrolled() {
		return yearEnrolled;
	}

	public void setYearEnrolled(String yearEnrolled) {
		this.yearEnrolled = yearEnrolled;
	}

	public DegreeDetails getDegreeDetails() {
		return degreeDetails;
	}

	public void setDegreeDetails(DegreeDetails degreeDetails) {
		this.degreeDetails = degreeDetails;
	}

	public Student()
	{
		super();
	}

	public Student(int id, String firstName, String lastName,String degreeName,String yearEnrolled,DegreeDetails degreeDetails) {
		this.degreeName= degreeName;
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.yearEnrolled= yearEnrolled;
		this.degreeDetails=degreeDetails;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString()
	{
		return "Employee [firstname=" + firstName + ", lastname=" + lastName +"]";
	}

}
