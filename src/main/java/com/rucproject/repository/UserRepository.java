package com.rucproject.repository;

import com.rucproject.model.Student;
import com.rucproject.model.User;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

import javax.swing.text.html.Option;

public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findByUsername(String username);
}
