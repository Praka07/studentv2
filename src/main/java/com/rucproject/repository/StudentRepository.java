package com.rucproject.repository;


import com.rucproject.model.Student;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface StudentRepository extends MongoRepository<Student, String>,QuerydslPredicateExecutor<Student> {
	Student findByFirstName(String firstName);
	Student findByLastName(String lastName);
	Student findById(int id);
	void deleteById(int id);
	List<Student> findByDegreeName(String name);

//	Page<Student> findByDegreeName(String name, Pageable page);
					
	List<Student> findByYearEnrolled(String year);
	List<Student> findByFirstNameStartsWith(String regexp);
	List<Student> findByLastNameStartsWith(String regexp);
			
	@Query("{'degreeDetails.careerPathLevelDetails.coreModulesTaken.yearOfModule':'?0'}")
	List<Student> findStudentByYearOfModule(String name);

	@Query("{'degreeDetails.careerPathLevelDetails.coreModulesTaken.moduleTitle':'?0'}")
	List<Student> findStudentByModuleTitle(String name);

}
