package com.rucproject.config;

import com.rucproject.config.jwt.JWTAuthenticationFilter;
import com.rucproject.config.jwt.JWTAuthorizationFilter;
import com.rucproject.service.SecureUserDetailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class CustomWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    @Autowired
    CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

    @Autowired
    SecureUserDetailService secureUserDetailService;

    //Starting Basic Authentication
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(secureUserDetailService);
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .httpBasic()
            .and()
            .authorizeRequests()
            .antMatchers("/user/**").permitAll()
            .antMatchers("/common").hasAnyAuthority("STUDENT", "HR", "PROFESSOR")
            .antMatchers("/student/**").hasAnyAuthority("HR")
            .anyRequest().authenticated();
    }
    //Ending Basic Authentication

    //Starting JWT authentication
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.cors().and().csrf().disable().authorizeRequests()
//            .antMatchers("/user/**").permitAll()
//            .antMatchers("/common/**").hasAnyAuthority("STUDENT", "HR", "PROFESSOR")
//            .antMatchers("/student/**").hasAnyAuthority("HR")
//            .anyRequest().authenticated()
//            .and()
//            .addFilter(new JWTAuthenticationFilter(authenticationManager()))
//            .addFilter(new JWTAuthorizationFilter(authenticationManager()))
//            // this disables session creation on Spring Security
//            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
//    }
//
//    @Override
//    public void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(secureUserDetailService).passwordEncoder(passwordEncoder());
//    }

    //Ending JWT authentication

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
