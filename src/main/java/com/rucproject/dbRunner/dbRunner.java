package com.rucproject.dbRunner;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.rucproject.service.StudentService;
import com.rucproject.model.DegreeDetails;
import com.rucproject.model.YearlyDetailofDegree;
import com.rucproject.model.Module;
import com.rucproject.model.Student;
import com.rucproject.repository.StudentRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;


@Component
@Service
public class dbRunner implements CommandLineRunner {
//	public dbRunner(StudentRepository studentRepository) {
//		this.studentRepository = studentRepository;
//	}

	@Autowired
	public StudentRepository studentRepository;
	@Autowired
	public StudentService studentService;

	public dbRunner(StudentService studentService) {
		this.studentService = studentService;

	}

		public void run(String... args) throws Exception {

//            studentRepository.deleteAll();

            LocalDate mathDATE = LocalDate.of(2010, 1, 3);
			LocalDate english1DATE =LocalDate.of(2011, 2, 17);
			LocalDate scienceDATE =LocalDate.of(2012, 3, 29);
			LocalDate economicsE =LocalDate.of(2013, 4, 28);

			LocalDate myDateObj = LocalDate.of(2000, 1, 01);
			System.out.println("Before formatting: " + myDateObj);
			DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			String formattedDate = myDateObj.format(myFormatObj);
			System.out.println("After formatting: " + formattedDate);

			LocalDate myDateObj2 = LocalDate.of(3000, 3, 20);
			System.out.println("Before formatting: " + myDateObj2);
			DateTimeFormatter myFormatObj2 = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			String formattedDate2 = myDateObj2.format(myFormatObj);

			Module networkEngineering = new Module("2008","Network Engineering", 85,mathDATE.plusDays(1),55,65 );
			Module advancedProgramming = new Module("2341","Advanced Programming", 85,mathDATE,55,65 );
			Module languageModule = new Module("2007","Language Module", 85,mathDATE,55,65 );
			Module ethicsModule = new Module("2003","Ethics Module", 85,mathDATE,55,65 );

			Module robotics = new Module("2003","Robotics", 85,mathDATE,55,65 );
			Module advancedNetworking = new Module("2004","Advanced Networking", 85,mathDATE,55,65);
			Module advancedLanguage = new Module("2005","Advanced Language", 85,mathDATE,55,65 );
			Module advandcedEthics = new Module("2006","Ethics Module", 85,mathDATE,55,65 );

			List<Module> jCoreModulesList = new ArrayList<Module>();
			jCoreModulesList.add(networkEngineering);
			jCoreModulesList.add(advancedProgramming);
			jCoreModulesList.add(languageModule);
			jCoreModulesList.add(ethicsModule);

			List<Module> jOptionalModulesList = new ArrayList<Module>();
			jOptionalModulesList.add(robotics);
			jOptionalModulesList.add(advancedNetworking);
			jOptionalModulesList.add(advancedLanguage);
			jOptionalModulesList.add(advandcedEthics);

			YearlyDetailofDegree jlevel100Details = new YearlyDetailofDegree("100", jCoreModulesList, jOptionalModulesList);
			YearlyDetailofDegree jlevel200Details = new YearlyDetailofDegree("200", jCoreModulesList, jOptionalModulesList);
			YearlyDetailofDegree jlevel300Details = new YearlyDetailofDegree("300", jCoreModulesList, jOptionalModulesList);
			List<YearlyDetailofDegree> jCareerPathLevelDetails=new ArrayList<YearlyDetailofDegree>();

			jCareerPathLevelDetails.add(jlevel100Details);
			jCareerPathLevelDetails.add(jlevel200Details);
			jCareerPathLevelDetails.add(jlevel300Details);

			Module networkEngineering2 = new Module("1980","Network Engineering", 85,myDateObj2,55,65);
			Module advancedProgramming2 = new Module("1976","Advanced Programming", 85,myDateObj.plusDays(1),55,65 );
			Module languageModule2 = new Module("1999","Language Module", 85,myDateObj.plusDays(2),55,65 );
			Module ethicsModule2 = new Module("2010","Ethics Module", 85,myDateObj.plusDays(3),55,65 );

			List<Module> eCoreModulesList = new ArrayList<Module>();
			eCoreModulesList.add(networkEngineering);
			eCoreModulesList.add(advancedProgramming);
			eCoreModulesList.add(languageModule);
			eCoreModulesList.add(ethicsModule);

			List<Module> eOptionalModulesList = new ArrayList<Module>();
			eOptionalModulesList.add(networkEngineering2);
			eOptionalModulesList.add(advancedProgramming2);
			eOptionalModulesList.add(languageModule2);
			eOptionalModulesList.add(ethicsModule2);

			YearlyDetailofDegree elevel100Details = new YearlyDetailofDegree("100", eCoreModulesList, eOptionalModulesList);
			YearlyDetailofDegree elevel200Details = new YearlyDetailofDegree("200", eCoreModulesList, eOptionalModulesList);
			YearlyDetailofDegree elevel300Details = new YearlyDetailofDegree("300", eCoreModulesList, eOptionalModulesList);
			List<YearlyDetailofDegree> eCareerPathLevelDetails=new ArrayList<YearlyDetailofDegree>();

			eCareerPathLevelDetails.add(elevel100Details);
			eCareerPathLevelDetails.add(elevel200Details);
			eCareerPathLevelDetails.add(elevel300Details);

			DegreeDetails jDegreeDetails=new DegreeDetails(jCareerPathLevelDetails);
			DegreeDetails eDegreeDetails=new DegreeDetails(eCareerPathLevelDetails);

			studentRepository.save(new Student(1, "JAY", "Z", "Computer Science","2008",jDegreeDetails));
			studentRepository.save(new Student(2, "JAZ", "O", "Computer Systems Engineering","2010",eDegreeDetails));
			studentRepository.save(new Student(3, "Damon", "Dash", "Robotics","2008",jDegreeDetails));
			studentRepository.save(new Student(4, "Big", "Kareem", "Big Data","2009",eDegreeDetails));

        	System.out.println("Students found with findAll()");
	        System.out.println("------------------------------");

	        for(Student Student: studentService.getAllStudents()) {
	        	System.out.println(Student.getFirstName());
				System.out.println(Student.getDegreeName());
	        }

		}
}


