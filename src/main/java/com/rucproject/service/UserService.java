package com.rucproject.service;

import com.rucproject.exception.UserAlreadyRegisteredException;
import com.rucproject.exception.config.ErrorCode;
import com.rucproject.model.User;
import com.rucproject.model.dto.UserCreateRequest;
import com.rucproject.repository.UserRepository;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User createUser(UserCreateRequest userCreateRequest) {
        Optional<User> userByUsername = userRepository.findByUsername(userCreateRequest.getUsername());
        if (userByUsername.isPresent()) {
            throw new UserAlreadyRegisteredException(ErrorCode.Application.USER_ALREADY_REGISTERED,
                "This username is already taken. Please register with different username");
        }
        User user = new User();
        BeanUtils.copyProperties(userCreateRequest, user);
        user.setPassword(passwordEncoder.encode(userCreateRequest.getPassword()));
        return userRepository.save(user);
    }

    public User readUserById (String id) {
        Optional<User> userbyId = userRepository.findById(id);
        return userbyId.orElse(null);
    }

    public User readUserByUsername (String username) {
        Optional<User> userbyId = userRepository.findByUsername(username);
        return userbyId.orElse(null);
    }

    public List<User> readUsers(Pageable pageable) {
        return userRepository.findAll(pageable).toList();
    }
}
