package com.rucproject.service;

import com.querydsl.core.types.Predicate;
import com.rucproject.model.QStudent;
import com.rucproject.model.Student;
import com.rucproject.repository.StudentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

//import com.rucproject.model.QStudent;

@Service
public class StudentService {

    @Autowired
    public StudentRepository studentRepository;


    public StudentService() {
        super();
    }

    public Student findByFirstName(String name) {
        return studentRepository.findByFirstName(name);
    }

    public Student findByLastName(String name) {
        return studentRepository.findByLastName(name);
    }

    public Page<Student> getAllStudentsPage(int page) {
    	Pageable firstPageWithTwoElements = PageRequest.of(page, 2);
    	
    	Page<Student> list1 = studentRepository.findAll(firstPageWithTwoElements);
//        List<Student> list1 = studentRepository.findAll();
        return list1;
    }
    
    public List<Student> getAllStudents() {
    
        List<Student> list1 = studentRepository.findAll();
        return list1;
    }

    public Student findById(int id) {
        Student Student = studentRepository.findById(id);
        return Student;
    }

    public List<Student> getStudentsByDegreeName(String name) {
        List<Student> Student = studentRepository.findByDegreeName(name);
        return Student;
    }

    public List<Student> getStudentsByYearEnrolled(String year) {
        List<Student> Student = studentRepository.findByYearEnrolled(year);
        return Student;
    }

    public List<Student> findByFirstNameStartsWith(String name) {
        List<Student> Student = studentRepository.findByFirstNameStartsWith(name);
        return Student;
    }

    public List<Student> findByLastNameStartsWith(String name) {
        List<Student> Student = studentRepository.findByLastNameStartsWith(name);
        return Student;
    }


    public Student insert(Student Student) {
        return studentRepository.insert(Student);
    }


    public void updateStudent(Student Student) {
        studentRepository.save(Student);
    }


    public void delete(int id) {
        studentRepository.deleteById(id);
    }


    public List<Student> findStudentByDegreeName(String name) {
        List<Student> Student = studentRepository.findByDegreeName(name);
        return Student;
    }

    public List<Student> findStudentByModuleTitle(String name) {
        List<Student> Student = studentRepository.findStudentByModuleTitle(name);
        return Student;
    }

    public List<Student> findStudentByCourseWorkPercentage(String mark) {
        QStudent qStudent = new QStudent("query");
        Predicate qDegreeDetailsPredicate = qStudent.degreeDetails.careerPathLevelDetails.any().coreModulesTaken.any().percentageOfCoursework.loe(Long.valueOf(mark));
        return (List<Student>) studentRepository.findAll(qDegreeDetailsPredicate);
    }

    public List<Student> findStudentByPercentageOfExam(String mark) {
        QStudent qStudent = new QStudent("query");
        Predicate qDegreeDetailsPredicate = qStudent.degreeDetails.careerPathLevelDetails.any().coreModulesTaken.any().percentageOfExam.loe(Long.valueOf(mark));
        return (List<Student>) studentRepository.findAll(qDegreeDetailsPredicate);
    }

    public List<Student> findStudentByCoreModuleDateExamTaken(String date) {
        QStudent qStudent = new QStudent("query");
        LocalDate dateTime = LocalDate.parse(date);
        Predicate qDegreeDetailsPredicate2 = qStudent.degreeDetails.careerPathLevelDetails.any().coreModulesTaken.any().dateTaken.eq(dateTime);
        return (List<Student>) studentRepository.findAll(qDegreeDetailsPredicate2);
    }

    public List<Student> findStudentByOptionalModuleDateExamTaken(String date) {
        QStudent qStudent = new QStudent("query");
        LocalDate dateTime = LocalDate.parse(date);
        Predicate qDegreeDetailsPredicate = qStudent.degreeDetails.careerPathLevelDetails.any().optionalModulesTaken.any().dateTaken.eq(dateTime);
        return (List<Student>) studentRepository.findAll(qDegreeDetailsPredicate);
    }

}
