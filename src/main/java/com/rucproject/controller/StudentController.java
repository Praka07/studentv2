package com.rucproject.controller;

import com.rucproject.exception.StudentCRUDException;
import com.rucproject.exception.StudentNotFoundException;
import com.rucproject.model.Student;
import com.rucproject.service.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Student Management System", description = "Operations pertaining to employee in Student Management System")
@RestController
@RequestMapping(value = "/student")
public class StudentController {

    @Autowired
    public StudentService studentService;

    @GetMapping
    @ResponseBody
    @ApiOperation(value = "Hello world system", response = String.class)
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String sayHello() {
//    	throw new StudentNotFoundException();
        return "Hello World";
    }


    @PostMapping
    @RequestMapping("/createStudent")
    public Student insert(@RequestBody Student Student) {
    	Student student=studentService.insert(Student);

        if(student==null) {
    		throw new StudentCRUDException("Student Exception Insert Faileed");
    	}
        return student;
    }

    @PutMapping
    @RequestMapping("/updateStudent")
    public void update(@RequestBody Student Student) {
        studentService.updateStudent(Student);
    }

    @DeleteMapping
    @RequestMapping("/deleteStudent/{id}")
    public void delete(@PathVariable(value = "id") int id) {
        studentService.delete(id);
    }


}
