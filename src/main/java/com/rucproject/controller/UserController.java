package com.rucproject.controller;

import com.rucproject.model.User;
import com.rucproject.model.dto.UserCreateRequest;
import com.rucproject.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "Read user by user ID", response = User.class)
    @GetMapping(value = "/{userId}")
    public ResponseEntity readUserById(@PathVariable("userId") String userId) {
        return ResponseEntity.ok(userService.readUserById(userId));
    }

    @ApiOperation(value = "Read user by username", response = User.class)
    @GetMapping("/search")
    public ResponseEntity readUserByUsername(@RequestParam(required = false) String username, Pageable pageable) {
        if (StringUtils.isEmpty(username)) {
            return ResponseEntity.ok(userService.readUsers(pageable));
        }
        return ResponseEntity.ok(userService.readUserByUsername(username));
    }

    @ApiOperation(value = "Create new user with valid credentials and role", response = User.class)
    @PostMapping
    public ResponseEntity createUser(@RequestBody UserCreateRequest request) {
        return ResponseEntity.ok(userService.createUser(request));
    }

}
