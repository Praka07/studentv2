package com.rucproject.controller;

import com.rucproject.exception.StudentNotFoundException;
import com.rucproject.model.Student;
import com.rucproject.service.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Common student read functions which will be shared among multiple roles.")
@RestController
@RequestMapping(value = "/common/student")
public class CommonStudentReaderController {
	
    @Autowired
    public StudentService studentService;

    @ApiOperation(value = "findByFirstName", response = Student.class)
    @GetMapping("/findByFirstName/{name}")
    public Student findByFirstName(@PathVariable(value = "name") String name) {
    	Student student=studentService.findByFirstName(name);
    	if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find First name");
    	}
//    	return studentService.findByFirstName(name);
        return student;
    }

    @GetMapping
    @RequestMapping("/findByLastName/{name}")
    public Student findByLastName(@PathVariable(value = "name") String name) {
    	Student student=studentService.findByLastName(name);
    	if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find Last name");
    	}
        return student;
    }

    @GetMapping
    @RequestMapping("/getAllStudents")
    public List<Student> getAllStudents() {
        return studentService.getAllStudents();
    }

    @GetMapping
    @RequestMapping("/getAllStudentsPage/{page}")
    public Page<Student> getAllStudentsPage(@PathVariable(value = "page") int page ) {
        return studentService.getAllStudentsPage(page);
    }
    
    @GetMapping
    @RequestMapping("/findById/{id}")
    public Student getStudentById(@PathVariable(value = "id") int id) {
    	Student student=studentService.findById(id);
    	if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find ID");
    	}
        return student;
    }

    @GetMapping
    @RequestMapping("/findByDegreeName/{name}")
    public List<Student> getStudentsByDegreeName(@PathVariable(value = "name") String name) {
    	List<Student> student=studentService.getStudentsByDegreeName(name);
    	if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find Last name");
    	}
    	return student;
    }

    @GetMapping
    @RequestMapping("/findByYearEnrolled/{year}")
    public List<Student> getStudentsByYearEnrolled(@PathVariable(value = "year") String year) {
    	List<Student> student=studentService.getStudentsByYearEnrolled(year);
    	if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find Year Enrolled");
    	}
    	return student;
    }

    @GetMapping
    @RequestMapping("/findByFirstNameStartsWith/{name}")
    public List<Student> findByFirstNameStartsWith(@PathVariable(value = "name") String name) {
    	List<Student> student=studentService.findByFirstNameStartsWith(name);
    	if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find student with the First name given");
    	}
    	return student;
    }

    //
    @GetMapping
    @RequestMapping("/findByLastNameStartsWith/{name}")
    public List<Student> findByLastNameStartsWith(@PathVariable(value = "name") String name) {
    	List<Student> student =studentService.findByLastNameStartsWith(name);
        if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find student with the Last name given ");
    	}
        return student;
    }

    @GetMapping
    @RequestMapping("/findStudentByDegreeName/{name}")
    public List<Student> findStudentByDegreeName(@PathVariable(value = "name") String name) {
    	List<Student> student =studentService.findStudentByDegreeName(name);
        if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find Last name");
    	}
        return student;
    }

    @GetMapping
    @RequestMapping("/findStudentByModuleTitle/{name}")
    public List<Student> findStudentByModuleTitle(@PathVariable(value = "name") String name) {
        List<Student> student =studentService.findStudentByModuleTitle(name);
        if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find Last name");
    	}
        return student;
    }

    @GetMapping
    @RequestMapping("/findStudentByCourseWorkPercentage/{mark}")
    public List<Student> findStudentByCourseWorkPercentage(@PathVariable(value = "mark") String mark) {
        List<Student> student =studentService.findStudentByCourseWorkPercentage(mark);
        if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant CourseWork");
    	}
        return student;
    }

    @GetMapping
    @RequestMapping("/findStudentByPercentageOfExam/{mark}")
    public List<Student> findStudentByPercentageOfExam(@PathVariable(value = "mark") String mark) {
        List<Student> student =studentService.findStudentByPercentageOfExam(mark);
        if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find student with that Percentage");
    	}
        return student;
    }

    @GetMapping
    @RequestMapping("/findStudentByCoreModuleDateExamTaken/{date}")
    public List<Student> findStudentByCoreModuleDateExamTaken(@PathVariable(value = "date") String date) {
        List<Student> student =studentService.findStudentByCoreModuleDateExamTaken(date);
        if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find student that took a Core Module exam on the date given");
    	}
        return student;
    }

    @GetMapping
    @RequestMapping("/findStudentByOptionalModuleDateExamTaken/{date}")
    public List<Student> findStudentByOptionalModuleDateExamTaken(@PathVariable(value = "date") String date) {
        List<Student> student =studentService.findStudentByOptionalModuleDateExamTaken(date);
        if(student==null) {
    		throw new StudentNotFoundException("Student Exception cant find student that took an Optional Module exam on the date given");
    	}
        return student;
    }


}
