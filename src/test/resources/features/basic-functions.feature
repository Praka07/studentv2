Feature: User roles and privileges test phase
  Scenario: Creating user setup

    When user register app_admin as ADMIN user with 123456789
    Then user app_admin should be present with role ADMIN

    When user register app_hr as HR user with 123456789
    Then user app_hr should be present with role HR

    When user register app_professor as PROFESSOR user with 123456789
    Then user app_professor should be present with role PROFESSOR

    When user register app_student as STUDENT user with 123456789
    Then user app_student should be present with role STUDENT

  Scenario: Registering already registered user

    When user register app_admin as ADMIN user with 123456789
    Then exception should be returned from core with status code 400

  Scenario: Validating HR user actions

    Given app_hr logs into the application with 123456789
    When user read student specific data
    Then valid response data should be present

    When user send create student request with John_Doe and 0712402
    Then valid user should be present


  Scenario: Validating student user actions

    Given app_student logs into the application with 123456789
    When user send create student request with John_Doe_student and 0712402
    Then exception should be returned from core with status code 403

    When user read student specific data
    Then valid response data should be present

  Scenario: Validating professor user actions

    Given app_professor logs into the application with 123456789
    When user send create student request with John_Doe_professor and 0712402
    Then exception should be returned from core with status code 403

    When user read student specific data
    Then valid response data should be present
