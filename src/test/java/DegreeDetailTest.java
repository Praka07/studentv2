import com.rucproject.model.DegreeDetails;
import com.rucproject.model.Module;
import com.rucproject.model.YearlyDetailofDegree;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class DegreeDetailTest {
    @Test
    public void listOfYearlyDetailOfDegreePositive() {
        List<YearlyDetailofDegree> careerPathLevelDetails = new ArrayList<>();
        YearlyDetailofDegree yearlyDetailofDegree = mock(YearlyDetailofDegree.class);
        careerPathLevelDetails.add(yearlyDetailofDegree);
        DegreeDetails degreeDetails = new DegreeDetails(careerPathLevelDetails);

        assertEquals(careerPathLevelDetails, degreeDetails.getCareerPathLevelDetails());
    }

    @Test
    public void listOfYearlyDetailOfDegreeNegative() {
        DegreeDetails degreeDetails = new DegreeDetails(null);
        assertNull(degreeDetails.getCareerPathLevelDetails());
    }

    @Test
    public void testYearlyDetailOfDegreeNullLevel() {
        ArrayList<Module> coreModulesTaken = new ArrayList<>();
        ArrayList<Module> optionalModulesTaken = new ArrayList<>();
        Module module = mock(Module.class);
        coreModulesTaken.add(module);
        YearlyDetailofDegree yearlyDetailOfDegree = new YearlyDetailofDegree(null, coreModulesTaken, optionalModulesTaken);
        assertNull(yearlyDetailOfDegree.getLevel());
    }

    @Test
    public void testYearlyDetailOfDegreeNullCoreModule() {
        ArrayList<Module> optionalModulesTaken = new ArrayList<>();
        YearlyDetailofDegree yearlyDetailOfDegree = new YearlyDetailofDegree("LEVEL_5", null, optionalModulesTaken);
        assertNull(yearlyDetailOfDegree.getCoreModulesTaken());
    }

    @Test
    public void testYearlyDetailOfDegreeNullOptionalModule() {
        ArrayList<Module> coreModulesTaken = new ArrayList<>();
        Module module = mock(Module.class);
        coreModulesTaken.add(module);
        YearlyDetailofDegree yearlyDetailOfDegree = new YearlyDetailofDegree("LEVEL_5", coreModulesTaken, null);
        assertNull(yearlyDetailOfDegree.getOptionalModulesTaken());
    }

    @Test
    public void testYearlyDetailOfDegreeValidLevel() {
        YearlyDetailofDegree yearlyDetailOfDegree = new YearlyDetailofDegree("LEVEL_5", null, null);
        assertEquals("LEVEL_5", yearlyDetailOfDegree.getLevel());
    }

    @Test
    public void testYearlyDetailOfDegreeValidCoreModule() {
        ArrayList<Module> coreModulesTaken = new ArrayList<>();
        Module module = mock(Module.class);
        coreModulesTaken.add(module);
        YearlyDetailofDegree yearlyDetailOfDegree = new YearlyDetailofDegree(null, coreModulesTaken, null);
        assertEquals(coreModulesTaken, yearlyDetailOfDegree.getCoreModulesTaken());
    }

    @Test
    public void testYearlyDetailOfDegreeValidOptionalModule() {
        ArrayList<Module> optionalModulesTaken = new ArrayList<>();
        YearlyDetailofDegree yearlyDetailOfDegree = new YearlyDetailofDegree(null, null, optionalModulesTaken);
        assertEquals(optionalModulesTaken, yearlyDetailOfDegree.getOptionalModulesTaken());
    }

    @Test
    public void testYearlyDetailOfDegreeValid() {
        ArrayList<Module> optionalModulesTaken = new ArrayList<>();
        ArrayList<Module> coreModulesTaken = new ArrayList<>();
        Module module = mock(Module.class);
        coreModulesTaken.add(module);
        YearlyDetailofDegree yearlyDetailOfDegree = new YearlyDetailofDegree("LEVEL_5", coreModulesTaken, optionalModulesTaken);
        assertEquals("LEVEL_5", yearlyDetailOfDegree.getLevel());
        assertEquals(optionalModulesTaken, yearlyDetailOfDegree.getOptionalModulesTaken());
        assertEquals(coreModulesTaken, yearlyDetailOfDegree.getCoreModulesTaken());
    }
}
