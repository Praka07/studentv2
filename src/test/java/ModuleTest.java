import com.rucproject.model.Module;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ModuleTest {
    @Test
    public void testModuleNullTitle() {
        Module module = new Module("asdf", null, 213, LocalDate.now(), 123, 2123);
        assertNotNull(module);
        assertNull(module.getModuleTitle());
    }

    @Test
    public void testModuleValidTitle() {
        Module module = new Module("asdf", "title", 213, LocalDate.now(), 123, 2123);
        assertNotNull(module);
        assertEquals("title", module.getModuleTitle());
    }

    @Test
    public void testModuleNullYearOfModule() {
        Module module = new Module(null, "title", 213, LocalDate.now(), 123, 2123);
        assertNotNull(module);
        assertNull(module.getYearOfModule());
    }

    @Test
    public void testModuleValidYearOfModule() {
        Module module = new Module("2018", "title", 213, LocalDate.now(), 123, 2123);
        assertNotNull(module);
        assertEquals("2018", module.getYearOfModule());
    }

    @Test
    public void testModuleNullPercentageAwarded() { //primitive datatype cannot be null
        Module module = new Module("2018", "title", 0, LocalDate.now(), 123, 2123);
        assertNotNull(module);
        assertEquals(0, module.getPercentageAwarded());
    }

    @Test
    public void testModuleValidPercentageAwarded() { //primitive datatype cannot be null
        Module module = new Module("2018", "title", 100, LocalDate.now(), 123, 2123);
        assertNotNull(module);
        assertEquals(100, module.getPercentageAwarded());
    }

    @Test
    public void testModuleNullLocalDate() { //primitive datatype cannot be null
        Module module = new Module("2018", "title", 100, null, 123, 2123);
        assertNotNull(module);
        assertNull(module.getDateTaken());
    }

    @Test
    public void testModuleValidLocalDate() { //primitive datatype cannot be null
        Module module = new Module("2018", "title", 100, LocalDate.now(), 123, 2123);
        assertNotNull(module);
        assertEquals(LocalDate.now(), module.getDateTaken());
    }

    @Test
    public void testModuleValidPercentageOfExam() { //primitive datatype cannot be null
        Module module = new Module("2018", "title", 100, LocalDate.now(), 50, 2123);
        assertNotNull(module);
        assertEquals(50, module.getPercentageOfExam());
    }

    @Test
    public void testModuleValidPercentageOfCoursework() { //primitive datatype cannot be null
        Module module = new Module("2018", "title", 100, LocalDate.now(), 50, 50);
        assertNotNull(module);
        assertEquals(50, module.getPercentageOfCoursework());
    }
}
