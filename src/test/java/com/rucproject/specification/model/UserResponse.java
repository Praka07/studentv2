package com.rucproject.specification.model;

public class UserResponse extends UserRequest{
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
