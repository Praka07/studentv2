package com.rucproject.specification;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features",
    plugin = {"pretty", "html:target/cucumber/ruc"},
    glue = "com.rucproject.specification.steps")
public class CucumberIntegrationTest {
}
