package com.rucproject.specification.steps;

import com.rucproject.model.User;
import com.rucproject.specification.model.UserRequest;
import com.rucproject.specification.model.UserResponse;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class AppHttpClient {

    private final String SERVER_URL = "http://localhost";
    private final String USER_API_ENDPOINT = "/user";
    private final String STUDENT_API_ENDPOINT = "/student/";
    private final String STUDENT_READ_API_ENDPOINT = "/common/student";

    private final RestTemplate restTemplate = new RestTemplate();
    @LocalServerPort
    private int port;

    private String appBase() {
        return SERVER_URL + ":" + port;
    }

    public User readUser(String userId) {
        return restTemplate.getForEntity(this.appBase() + USER_API_ENDPOINT + "/" + userId, User.class).getBody();
    }

    public User registerUser(User user) throws HttpStatusCodeException {
        return restTemplate.postForEntity(this.appBase() + USER_API_ENDPOINT, user, User.class).getBody();
    }

    public User readUserByUsername(String username) {
        return restTemplate.getForEntity(this.appBase() + USER_API_ENDPOINT + "/search?username=" + username, User.class).getBody();
    }
    HttpHeaders createHeaders(final String username, final String password) {
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("US-ASCII")));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }

    public String readStudent(String username, String password) throws HttpStatusCodeException {
        return restTemplate.exchange(this.appBase() + STUDENT_READ_API_ENDPOINT + "/getAllStudents",
            HttpMethod.GET, new HttpEntity<>(createHeaders(username, password)), String.class).getBody();
    }

    public UserResponse createStudent (UserRequest userRequest, String username, String password)  throws HttpStatusCodeException {
        HttpEntity<UserRequest> request = new HttpEntity<>(userRequest, createHeaders(username, password));
        return restTemplate.exchange(this.appBase()+STUDENT_API_ENDPOINT+"/createStudent", HttpMethod.POST, request, UserResponse.class).getBody();
    }

    public String updateStudent (String fName, String lName) {
        UserRequest userRequest = new UserRequest();
        userRequest.setFirstName(fName);
        userRequest.setLastName(lName);
        HttpEntity<UserRequest> request = new HttpEntity<>(userRequest);
        return restTemplate.postForEntity(this.appBase()+STUDENT_API_ENDPOINT+"/createStudent", request, String.class).getBody();
    }



}
