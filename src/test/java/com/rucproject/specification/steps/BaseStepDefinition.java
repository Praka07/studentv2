package com.rucproject.specification.steps;

import com.rucproject.model.User;
import com.rucproject.specification.model.UserRequest;
import com.rucproject.specification.model.UserResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpStatusCodeException;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class BaseStepDefinition {
    @Autowired
    private AppHttpClient appHttpClient;
    private HttpStatusCodeException httpStatusCodeException;
    private String username;
    private String password;
    private String tmpResponse;
    private UserResponse userResponse;

    @When("user register {word} as {word} user with {word}")
    public void registerUser(String username, String role, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setRole(role);
        try {
            appHttpClient.registerUser(user);
        } catch (HttpStatusCodeException e) {
            httpStatusCodeException = e;
        }
    }

    @Then("user {word} should be present with role {word}")
    public void validateUsername(String username, String rolename) {
        User user = appHttpClient.readUserByUsername(username);

        assert user != null;
        assert user.getUsername().equals(username);
        assert user.getId() != null;
        assert user.getPassword() != null;
        assert user.getRole().equals(rolename);

    }

    @Then("exception should be returned from core with status code {int}")
    public void validateExceptionCode (int statusCode) {
        assert httpStatusCodeException.getRawStatusCode() == statusCode;
    }

    @Given("{word} logs into the application with {word}")
    public void setBasicAuthenticationWithUser (String username, String password) {
        this.username = username;
        this.password = password;

    }

    @When("user read student specific data")
    public void readStudentAPI () {
        try {
            tmpResponse = appHttpClient.readStudent(this.username, this.password);
        } catch (HttpStatusCodeException e) {
            httpStatusCodeException = e;
        }
    }

    @Then("valid response data should be present")
    public void validateResponse () {
        assert tmpResponse != null;
    }

    @When("user send create student request with {word} and {word}")
    public void createStudent (String fname, String lName) {
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setFirstName(fname);
            userRequest.setLastName(lName);
            userResponse = appHttpClient.createStudent(userRequest, this.username, this.password);
        } catch (HttpStatusCodeException e) {
            httpStatusCodeException = e;
        }
    }

    @Then("valid user should be present")
    public void validateStudent () {
        assert userResponse.getId() != null;
        assert userResponse.getFirstName() != null;
        assert userResponse.getLastName() != null;
    }

}
