import com.rucproject.service.StudentService;
import com.rucproject.model.Student;
import com.rucproject.repository.StudentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UnitTest {
    @Mock
    public StudentRepository studentRepository;
    @InjectMocks
    public StudentService studentService;

    @Test
    public void testFindByFirstNameNull() {
        assertNull(studentService.findByFirstName(null));
    }

    @Test
    public void testFindByFirstNameValid() {
        Student student = new Student(1, "John", "Doe", "BS", "2012", null);
        when(studentRepository.findByFirstName("John")).thenReturn(student);
        assertEquals(student, studentService.findByFirstName("John"));
    }

    @Test
    public void testFindByLastNameValid() {
        Student student = new Student(1, "John", "Doe", "BS", "2012", null);
        when(studentRepository.findByLastName("Doe")).thenReturn(student);
        assertEquals(student, studentService.findByLastName("Doe"));
    }

    @Test
    public void testFindByLastNameNull() {
        when(studentRepository.findByLastName(null)).thenReturn(null);
        assertNull(studentService.findByLastName(null));
    }

    @Test
    public void testGetAllStudents() {
        List<Student> list1 = new ArrayList<>();
        Student student1 = new Student(1, "John", "Doe", "BS", "2012", null);
        Student student2 = new Student(2, "Joan", "Harris", "BS", "2012", null);
        list1.add(student1);
        list1.add(student2);
        when(studentRepository.findAll()).thenReturn(list1);
        assertEquals(list1, studentService.getAllStudents());
    }

    @Test
    public void testGetStudentsById() {
        Student student1 = new Student(1, "John", "Doe", "BS", "2012", null);
        when(studentRepository.findById(1)).thenReturn(student1);
        assertEquals(student1, studentService.findById(1));
    }

    @Test
    public void testGetStudentsByDegreeName() {
        List<Student> list1 = new ArrayList<>();
        Student student1 = new Student(1, "John", "Doe", "BS", "2012", null);
        Student student2 = new Student(2, "Joan", "Harris", "BS", "2012", null);
        list1.add(student1);
        list1.add(student2);
        when(studentRepository.findByDegreeName("BS")).thenReturn(list1);
        assertEquals(list1, studentService.findStudentByDegreeName("BS"));
    }

    @Test
    public void testGetStudentsByYearEnrolled() {
        List<Student> list1 = new ArrayList<>();
        Student student1 = new Student(1, "John", "Doe", "BS", "2012", null);
        Student student2 = new Student(2, "Joan", "Harris", "BS", "2012", null);
        list1.add(student1);
        list1.add(student2);
        when(studentRepository.findByYearEnrolled("2012")).thenReturn(list1);
        assertEquals(list1, studentService.getStudentsByYearEnrolled("2012"));
    }

    @Test
    public void testFindByFirstNameStartsWith() {
        List<Student> list1 = new ArrayList<>();
        Student student1 = new Student(1, "John", "Doe", "BS", "2012", null);
        Student student2 = new Student(2, "Joan", "Harris", "BS", "2012", null);
        list1.add(student1);
        list1.add(student2);
        when(studentRepository.findByFirstNameStartsWith("J")).thenReturn(list1);
        assertEquals(list1, studentService.findByFirstNameStartsWith("J"));
    }

    @Test
    public void testFindByLastNameStartsWith() {
        List<Student> list1 = new ArrayList<>();
        Student student1 = new Student(1, "John", "Doe", "BS", "2012", null);
        Student student2 = new Student(2, "Joan", "Dan", "BS", "2012", null);
        list1.add(student1);
        list1.add(student2);
        when(studentRepository.findByLastNameStartsWith("D")).thenReturn(list1);
        assertEquals(list1, studentService.findByLastNameStartsWith("D"));
    }

    //    @Test
//    public void testCreateStudent() {
//        Student student1 = new Student(1, "John", "Doe", "BS", "2012", null);
//        when(studentRepository.insert(student1)).thenReturn(student1);
//        assertEquals(student1, studentService.insert(new Student(1, "John", "Doe", "BS", "2012", null)));
//    }

}
